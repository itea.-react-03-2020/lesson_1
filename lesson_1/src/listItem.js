import React from 'react';

const ListItem = ({ user, handler }) => {
    return(
        <li
            className={ user.arrived ? "success" : "error"}
        >
            <span>{user.name}</span>
            <button 
                onClick={handler(user)}
            > 
                Arrived! 
            </button>
        </li>
    );

}

export default ListItem;


