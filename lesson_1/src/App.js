import React, { Component } from 'react';

import ListItem from './listItem';

import data from './guests.json';
import './App.css';


class App extends Component{

	constructor( props ){
		super( props );
		this.state = {
			query: "",
			filtredData: null,
			data: data.map( user => {
				user.arrived = false;
				return user;
			})
		}
	}

	makeUserArrived = user => e => {
		const { data } = this.state;
		const changedData = data.map( item => {
			if( item.index === user.index){
				item.arrived = !item.arrived;
			}
			return item;
		});

		this.setState({
			data: changedData
		})
	}
	
	handleChange = (e) => {
		const query = e.target.value;
		const filtredData = this.state.data.filter( item =>{
			return item.name.indexOf( query ) !== -1;
		});

		this.setState({
			query: query,
			filtredData
		});
	}

	render = () => {
		const { data, query, filtredData } = this.state;
		const { makeUserArrived, handleChange } = this;

		let listData = data;

		if( filtredData !== null ){
			listData = filtredData;
		}

		return(
			<div>
				<input 
					onChange={handleChange}
					value={query}
				/>
				<ul>
				{
					listData.map( (item, key) => {
						return( 
							<ListItem 
								key={key}
								user={item}
								handler={makeUserArrived}
							/>
						);
					})
				}
				</ul>	
			</div>
		)
	}
}

export default App;
