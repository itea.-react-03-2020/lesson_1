/*
	Императивное и декларативное программирование
*/

// Пример императивного кода, который описывает как что либо работает
const toLowerCaseImp = input => {
	const output = [];                       // Создай новый пустой массив
	for (let i = 0; i < input.length; i++) { // Перебери входящий массив
		output.push(input[i].toLowerCase());   // Добавь в массив новый элементы со старого в которых уменьшены буквы
	}
	return output;                           // Верни результат
};

// Пример декларативного кода, который описывает какой результат мы хотим получить
const toLowerCaseDec = input => input.map(
	value => value.toLowerCase()
);

// Imperative
const map = new google.maps.Map(document.getElementById('map'), {
	zoom: 4,
	center: myLatLng,
});
const marker = new google.maps.Marker({
	position: myLatLng,
	title: 'Hello World!',
});
marker.setMap(map);

// Declarative
<Gmaps zoom={4} center={myLatLng}>
	<Marker position={myLatLng} Hello world! />
</Gmaps>



//  React Classes types ( ES6 vs ES7 )

/*
	ES7
*/
// class App extends Component {
// 	state = {
// 		myStateData: 1
// 	} 
// 	someStuff = (event) => {
// 		console.log( event )
// 	}
// 	render = () => {
// 		// <div>
// 		// 	myApp
// 		// </div>
// 	}
// }

/*
	ES6
*/
// class App extends Component {
// 	constructor(props){
// 		super(props);
// 		this.state = {
// 			myStateData: 1
// 		}
// 		this.someStuff = this.someStuff.bind(this);
// 	}
// 	someStuff(event){
// 		console.log( event );
// 	}
// 	render() {
// 		// ...
// 	}
// }

/*
	React 13.0
*/
// React.createClass({
// 	getInitialState : function() {
// 		return {
// 		name : "Petro"
// 		};
// 	},
// 	render: function(){
// 		return(
// 		<div>
// 			old stuff
// 		</div>
// 		);
// 	}
// });

